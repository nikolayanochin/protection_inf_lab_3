<?php
/**
 * Created with love by АльянсЭкспресс.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 19.11.2018
 * Time: 14:33
 */

require('../autoload.php');

$username = $_POST['username'] ?? null;
$salt = $_POST['salt'] ?? null;
$password_verifier = $_POST['password_verifier'] ?? null;


if ($username && $salt && $password_verifier) {

    $data = [
        'salt' => $salt,
        'password_verifier' => $password_verifier
    ];

    file_put_contents(__DIR__ . '/../db/' . $username . '.json', json_encode($data));
    echo "{$username} зарегистрирован!";
    exit;
}

echo 'Данные с ошибкой!';

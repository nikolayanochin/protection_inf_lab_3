<?php
/**
 * Created with love by АльянсЭкспресс.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 19.11.2018
 * Time: 14:33
 */
namespace server;

require('../autoload.php');

use lib\HashHelper;

$username = $_POST['username'] ?? null;
$client_verifier = $_POST['client_verifier'] ?? null;
$step = $_POST['step'] ?? null;
$client_M = $_POST['client_M'] ?? null;
$response = [];
$hashHelper = new HashHelper();


if ($username && $client_verifier && $step == 1 && $client_verifier != 0) {
    $path = __DIR__ . '/../db/' . $username . '.json';
    if (file_exists($path)) {
        $data = json_decode(file_get_contents($path));

        $b = rand(1, 1000);

        $part1 = gmp_strval(gmp_mul($hashHelper->k, $data->password_verifier));
        $part2 = gmp_strval(gmp_mod(gmp_pow($hashHelper->g, $b), $hashHelper->n));
        $server_verifier = gmp_strval(gmp_mod(gmp_add($part1, $part2), $hashHelper->n));

        $response = [
            'salt' => $data->salt,
            'server_verifier' => $server_verifier,
        ];

        $scram = $hashHelper->hash($client_verifier . $server_verifier);

        if ($scram == 0) {
            $response['error'] = true;
        }

        $a1 = gmp_strval(gmp_pow($data->password_verifier, $scram));
        $b1 = gmp_strval(gmp_mod($a1, $hashHelper->n));
        $c1 = gmp_strval(gmp_mul($client_verifier, $b1));
        $d1 = gmp_strval(gmp_pow($c1, $b));
        $S = gmp_strval(gmp_mod($d1, $hashHelper->n));

        $hash_S = $hashHelper->hash($S);
        $data = [
            'hash_S' => $hash_S,
            'server_verifier' => $server_verifier,
            'client_verifier' => $client_verifier,
            'salt' => $data->salt,
        ];
        file_put_contents(__DIR__ . '/../db/step2/' . $username . '.json', json_encode($data));

        echo json_encode($response);
        exit;
    }
} else if ($username && $client_M && $step == 2) {
    $path = __DIR__ . '/../db/step2/' . $username . '.json';
    if (file_exists($path)) {
        $data = json_decode(file_get_contents($path));

        $M = $hashHelper->hash(gmp_xor($hashHelper->hash($hashHelper->n), $hashHelper->hash($hashHelper->g))
            . $hashHelper->hash($username)
            . $data->salt . $data->client_verifier . $data->server_verifier . $data->hash_S);

        if ($client_M != $M) {
            $response['error'] = true;
            echo json_encode($response);
            exit;
        }

        $R = $hashHelper->hash($data->client_verifier . $M . $data->hash_S);
        $response['R'] = $R;
    }
} else {
    $response['error'] = true;
}
echo json_encode($response);

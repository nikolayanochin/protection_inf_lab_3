<?php
/**
 * Created with love by АльянсЭкспресс.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 19.11.2018
 * Time: 14:34
 */
namespace client;

require('../autoload.php');

use lib\HashHelper;

$hashHelper = new HashHelper();

$username = 'test_user';
$password = 'test_password';

$salt = $hashHelper->randomString();
$hash = $hashHelper->hash($salt . $password);


//s = random_string()
//x = H(s, p)
//v = g^x % N
    
$pow = gmp_strval(gmp_pow($hashHelper->g, $hash));
$password_verifier = gmp_mod($pow, $hashHelper->n);


$endpoint = 'http://srp6.local/server/registration.php';

$data = [
    'username' => $username,
    'salt' => $salt,
    'password_verifier' => gmp_strval($password_verifier)
];

$response = $hashHelper->request($endpoint, $data);

echo "Сервер ответил: " . $response;

?>



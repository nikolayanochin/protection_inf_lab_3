<?php
/**
 * Created with love by АльянсЭкспресс.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 19.11.2018
 * Time: 14:33
 */
namespace client;

require('../autoload.php');

use lib\HashHelper;

$hashHelper = new HashHelper();

$username = 'test_user';
$password = 'test_password';

$rand = rand(1, 1000);
$client_verifier = gmp_strval(gmp_mod(gmp_pow($hashHelper->g, $rand), $hashHelper->n));

$endpoint = 'http://srp6.local/server/auth.php';

$data = [
    'username' => $username,
    'client_verifier' => $client_verifier,
    'step' => 1
];

$response = json_decode($hashHelper->request($endpoint, $data));

if (!empty($response->error)) {
    echo 'Данные с ошибкой!';
    exit;
}

$scram = $hashHelper->hash($client_verifier . $response->server_verifier);

if ($scram == 0) {
    echo 'Данные с ошибкой!';
    exit;
}

$x = $hashHelper->hash($response->salt . $password);

$a1 = gmp_strval(gmp_pow($hashHelper->g, $x));
$b1 = gmp_strval(gmp_mod($a1, $hashHelper->n));
$c1 = gmp_strval(gmp_mul($hashHelper->k, $b1));
$d1 = gmp_strval(gmp_abs(gmp_sub($response->server_verifier, $c1)));
$e1 = gmp_strval(gmp_mul($scram, $x));
$f1 = gmp_strval(gmp_add($rand, $e1));
$g1 = gmp_strval(gmp_pow($d1, $f1));
$S = gmp_strval(gmp_mod($g1, $hashHelper->n));

$hash_S = $hashHelper->hash($S);


$M = $hashHelper->hash(gmp_xor($hashHelper->hash($hashHelper->n), $hashHelper->hash($hashHelper->g))
    . $hashHelper->hash($username)
    . $response->salt . $client_verifier . $response->server_verifier . $hash_S);

$data = [
    'username' => $username,
    'client_M' => $M,
    'step' => 2
];

$response = json_decode($hashHelper->request($endpoint, $data));

if (!empty($response->error)) {
    echo 'Данные с ошибкой!';
    exit;
}

$R = $hashHelper->hash($client_verifier . $M . $hash_S);

if ($R == $response->R) {
    echo 'УРА! Мы залогинены';
} else {
    echo 'Данные с ошибкой!';
}

?>
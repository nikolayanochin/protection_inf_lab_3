<?php
/**
 * Created with love by АльянсЭкспресс.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 19.11.2018
 * Time: 14:47
 */


namespace lib;


class HashHelper
{
    public $k = 3; // всегда

    public $g = 11;
    public $n = 1907;

    public function randomString($length = 10)
    {
        $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $rand_string = '';
        for ($i = 0; $i < $length; $i++) {
            $rand_string .= $characters[rand(0, strlen($characters) - 1)];
        }
        return $rand_string;
    }

    public function request($url, $data)
    {
        $myCurl = curl_init();

        curl_setopt_array($myCurl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => http_build_query($data)
        ));
        $response = curl_exec($myCurl);
        curl_close($myCurl);
        return $response;
    }


    public function hash($str) {
        $int = abs(crc32($str));
        while($int > 32) {
            $int = ($int >> 1);
        }
        return $int;
    }
}